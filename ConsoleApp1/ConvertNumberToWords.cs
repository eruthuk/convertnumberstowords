﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class ConvertNumberToWords
    {
        public string GetNumberInEnglish(int number)
        {
            if ((number <= 0) || (number > 9999))
            {
                return "not in range";
            }

            var sb = new StringBuilder();

            var numLength = number.ToString().Length;

            switch (numLength)
            {
                case 1: //single digit number
                    sb.Append(CalculateSingleDigitNumber(number));
                    break;
                case 2:
                    sb.Append(CalculateDoubleDigitNumber(number));
                    break;
                case 3:
                    sb.Append(CalculateThreeDigitNumber(number));
                    break;
                case 4:
                    sb.Append(CalculateFourDigitNumber(number));
                    break;
            }

            return sb.ToString();
        }

        private string CalculateFourDigitNumber(int number)
        {
            var myNum1 = Convert.ToInt32(number.ToString().Substring(0, 1));
            var myNum2 = Convert.ToInt32(number.ToString().Substring(1, 1));
            var myNum3 = Convert.ToInt32(number.ToString().Substring(2, 1));
            var myNum4 = Convert.ToInt32(number.ToString().Substring(3, 1));

            var twoDigitNum = Convert.ToInt32(number.ToString().Substring(2, 2));
            var threeDigitNum= Convert.ToInt32(number.ToString().Substring(1, 3));

            var sb = new StringBuilder();

            sb.Append(Enum.GetName(typeof(NumberDetails.SingleDigits), myNum1));
            sb.Append(" Thousand");

            switch (myNum2)
            {
                case 0:
                    sb.Append(" And ");
                    if (myNum3 == 0)
                    {
                        sb.Append(CalculateSingleDigitNumber(myNum4));
                    }
                    else
                    {
                        sb.Append(CalculateDoubleDigitNumber(twoDigitNum));
                    }
                    break;
                default:
                    sb.Append(" ");
                    sb.Append(CalculateThreeDigitNumber(threeDigitNum));
                    break;
            }

            return sb.ToString();
        }

        private string CalculateThreeDigitNumber(int number)
        {
            var myNum1 = Convert.ToInt32(number.ToString().Substring(0, 1));
            var myNum2 = Convert.ToInt32(number.ToString().Substring(1, 1));
            var myNum3 = Convert.ToInt32(number.ToString().Substring(2, 1));

            var sb = new StringBuilder();

            sb.Append(Enum.GetName(typeof(NumberDetails.SingleDigits), myNum1));
            sb.Append(" Hundred And ");

            switch (myNum2)
            {
                case 0:
                    sb.Append(Enum.GetName(typeof(NumberDetails.SingleDigits), myNum3));
                    break;
                case 1:
                    sb.Append(Enum.GetName(typeof(NumberDetails.TenAndTeens), myNum3));
                    break;
                default:
                    sb.Append(Enum.GetName(typeof(NumberDetails.TwentyToNinety), myNum2));
                    sb.Append(" ");
                    sb.Append(Enum.GetName(typeof(NumberDetails.SingleDigits), myNum3));
                    break;
            }

            return sb.ToString();
        }

        private string CalculateDoubleDigitNumber(int number)
        {
            if (number.ToString().StartsWith("1"))
            {
                // number is 10 or in the teens
                var myNum = Convert.ToInt32(number.ToString().Substring(1));
                return Enum.GetName(typeof(NumberDetails.TenAndTeens), myNum);
            }
            else
            {
                var myNum1 = Convert.ToInt32(number.ToString().Substring(0, 1));
                var myNum2 = Convert.ToInt32(number.ToString().Substring(1, 1));

                return Enum.GetName(typeof(NumberDetails.TwentyToNinety), myNum1) + " "
                    + Enum.GetName(typeof(NumberDetails.SingleDigits), myNum2);
            }

        }

        private string CalculateSingleDigitNumber(int number)
        {
            return Enum.GetName(typeof(NumberDetails.SingleDigits), number);
        }


    }
}
