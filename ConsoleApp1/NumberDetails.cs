﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class NumberDetails
    {
        public enum SingleDigits
        {
            One=1,
            Two=2,
            Three=3,
            Four=4,
            Five=5,
            Six=6,
            Seven=7,
            Eight=8,
            Nine=9
        }

        public enum TenAndTeens
        {
            Ten,
            Eleven,
            Twelve,
            Thirteen,
            Fourteen,
            Fifteen,
            Sixteen,
            Seventeen,
            Eighteen,
            Nineteen
        }

        public enum TwentyToNinety
        {
            Twenty=2,
            Thirty=3,
            Fourty=4,
            Fifty=5,
            Sixty=6,
            Seventy=7,
            Eighty=8,
            Ninety=9
        }
    }
}
