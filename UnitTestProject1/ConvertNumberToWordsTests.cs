﻿using ConsoleApp1;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestFixture]
    public class ConvertNumberToWordsTests
    {
        [Test]
        public void Input1ReturnsOneTest()
        {
            // arrange
            var inputVal = 1;
            var expected = "One";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(-1)]
        [TestCase(10101)]
        public void InputOutOfRangeGivesErrorTest(int inputVal)
        {
            // arrange
            var expected = "not in range";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Input2Returns2Test()
        {
            // arrange
            var inputVal = 2;
            var expected = "Two";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs10ReturnsTenTest()
        {
            // arrange
            var inputVal = 10;
            var expected = "Ten";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs15ReturnsFifteenTest()
        {
            // arrange
            var inputVal = 15;
            var expected = "Fifteen";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs25ReturnsTwentyFiveTest()
        {
            // arrange
            var inputVal = 25;
            var expected = "Twenty Five";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs307ReturnsThreeHundredAndSeven()
        {
            // arrange
            var inputVal = 307;
            var expected = "Three Hundred And Seven";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs613ReturnsSixHundredAndThirteen()
        {
            // arrange
            var inputVal = 613;
            var expected = "Six Hundred And Thirteen";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs585ReturnsFiveHundredAndEightyFive()
        {
            // arrange
            var inputVal = 585;
            var expected = "Five Hundred And Eighty Five";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs1001ReturnsOneThousandAndOne()
        {
            // arrange
            var inputVal = 1001;
            var expected = "One Thousand And One";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs2014ReturnsTwoThousandAndFourteen()
        {
            // arrange
            var inputVal = 2014;
            var expected = "Two Thousand And Fourteen";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs4079ReturnsFourThousandAndSeventyNine()
        {
            // arrange
            var inputVal = 4079;
            var expected = "Four Thousand And Seventy Nine";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs5101ReturnsFiveThousandOneHundredAndOne()
        {
            // arrange
            var inputVal = 5101;
            var expected = "Five Thousand One Hundred And One";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs6313ReturnsSixThousandThreeHundredAndThirteen()
        {
            // arrange
            var inputVal = 6313;
            var expected = "Six Thousand Three Hundred And Thirteen";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void InputIs8765ReturnsEightThousandSevenHundredAndSixtyFive()
        {
            // arrange
            var inputVal = 8765;
            var expected = "Eight Thousand Seven Hundred And Sixty Five";
            var conv = new ConvertNumberToWords();

            // act
            var actual = conv.GetNumberInEnglish(inputVal);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}
